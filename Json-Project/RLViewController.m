//
//  RLViewController.m
//  Json-Project
//
//  Created by dcl17 on 21/01/14.
//  Copyright (c) 2014 dcl17. All rights reserved.
//

#import "RLViewController.h"
#import "RLDetailsViewController.h"

static NSString *liga = @"https://api.foursquare.com/v2/venues/search?radius=800&locale=es&v=20140121&client_secret=OV3JVHFAU32WALYUZ1PJOHUDU2PXP5E5CUEQC2QJC5JAKWPO&client_id=QZVLKFPQ5MNXF3ISLUNYVBTEENWKEMZ3FZMEOKEAIYW1FWLS";

@interface RLViewController ()<UITableViewDataSource, UITableViewDelegate, CLLocationManagerDelegate>
@property (strong, nonatomic) NSArray *venues;
@property (weak , nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) NSIndexPath *lastSelectedIndexPath;
@property (weak, nonatomic) UIRefreshControl *refreshControl;
@property (nonatomic, readonly) CLLocationManager *locationManager;
@property (strong,nonatomic) CLLocation *lastLocation;
@end

@implementation RLViewController
@synthesize locationManager = _locationManager;

#pragma mark -Getter Selectors
-(CLLocationManager *)locationManager{
    if (!_locationManager) {
        _locationManager = [[CLLocationManager alloc]init];
        _locationManager.desiredAccuracy = kCLLocationAccuracyBest;
        _locationManager.headingFilter = kCLHeadingFilterNone;
        _locationManager.distanceFilter = 20.0;
        _locationManager.delegate = self;
    }
    return _locationManager;
}


#pragma mark - Implementation Refresh Control
-(void)customizeViewInterface{
    self.title = @"Venues";
    UIRefreshControl *refreshControl =[[UIRefreshControl alloc] init];
    [refreshControl addTarget:self
                       action:@selector(refreshInfo:) forControlEvents:UIControlEventValueChanged];
    [self.tableView addSubview:refreshControl];
    self.refreshControl = refreshControl;
    
}

-(void)refreshInfo:(UIRefreshControl *)sender{
   NSString *venueURLString = [NSString stringWithFormat:@"%@&ll=%g,%g", liga, self.lastLocation.coordinate.latitude, self.lastLocation.coordinate.longitude];
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithURL:[NSURL URLWithString:venueURLString] completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        NSLog(@"%@", json);
        self.venues = [json valueForKeyPath:@"response.venues"];
        if (!error) {
            NSError *jsonError;
            self.venues = [[NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&jsonError] valueForKeyPath:@"response.venues"];
            [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                [self.tableView reloadData];
                [sender endRefreshing];
            }];
        }
        else{
            NSLog(@"Hubo un error %@", [error localizedDescription]);
            [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                [sender endRefreshing];
                UIAlertView *alert =[[UIAlertView alloc] initWithTitle:@"Venues" message:@"No fue posible obtener la informacion" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
                [alert show];
            }];
        }
    }];
    [dataTask resume];
}

- (void)viewDidLoad{
    [super viewDidLoad];
    [self customizeViewInterface];
    
}

#pragma mark - UITableViewDataSource Selectors
-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{return self.venues.count;}

-(UITableViewCell*) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *cellIdentifier = @"cellIdentifier";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier: cellIdentifier];
    if(!cell){
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier];
    }
    NSDictionary *venueInfo =self.venues[indexPath.row];
    cell.backgroundColor = [UIColor clearColor];
    cell.textLabel.text = venueInfo[@"name"];
    cell.detailTextLabel.text = [venueInfo
                                 valueForKeyPath:@"location.address"];
    cell.accessoryType= UITableViewCellAccessoryDisclosureIndicator;
    cell.textLabel.textColor = [UIColor whiteColor];
    return cell;
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.tableView deselectRowAtIndexPath:self.lastSelectedIndexPath animated:YES];
    [self.locationManager startUpdatingLocation];
    
    if (!self.venues.count){
        [self.refreshControl beginRefreshing];
        [self refreshInfo:self.refreshControl];
    }
}
-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations{
    CLLocation *lastLocation = [locations lastObject];
    if (!self.lastLocation) {
        self.lastLocation =lastLocation;
        [self.tableView setContentOffset:CGPointMake(0.0, -90.0) animated:YES];
        [self.refreshControl beginRefreshing];
        [self refreshInfo:self.refreshControl];
    }
    else
        self.lastLocation = lastLocation;
}

#pragma mark - Actions Selectors
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *venueIdentifier= self.venues[indexPath.row][@"id"];
    NSDictionary *selectedVenue = self.venues[indexPath.row];
    NSLog(@"Se selecciono: > %@", venueIdentifier);
    RLDetailsViewController *detailsController = [[RLDetailsViewController alloc] initWithNibName:@"RLDetailsViewController" bundle:nil];
    [self.navigationController pushViewController:detailsController animated:YES];
    self.lastSelectedIndexPath = indexPath;
    detailsController.idUser = venueIdentifier;
    detailsController.latitude = [[selectedVenue valueForKeyPath:@"location.lat"] floatValue];
    detailsController.longitude = [[selectedVenue valueForKeyPath:@"location.lng"] floatValue];
}

@end
