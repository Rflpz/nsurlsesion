//
//  RLDetailsViewController.m
//  Json-Project
//
//  Created by dcl17 on 22/01/14.
//  Copyright (c) 2014 dcl17. All rights reserved.
//

#import "RLDetailsViewController.h"
#import "UIImageView+AFNetworking.h"
#import "RLVenueAnnotaion.h"


static NSString *url_1 = @"https://api.foursquare.com/v2/venues/";
static NSString *url_2 = @"?v=20140121&client_secret=OV3JVHFAU32WALYUZ1PJOHUDU2PXP5E5CUEQC2QJC5JAKWPO&client_id=QZVLKFPQ5MNXF3ISLUNYVBTEENWKEMZ3FZMEOKEAIYW1FWLS";

@interface RLDetailsViewController ()
@property (weak, nonatomic) NSString *urlDescription;
@property (weak, nonatomic) NSString *urlImage;
@property (strong,nonatomic) NSDictionary *description;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *addressLabel;
@property (weak, nonatomic) IBOutlet UILabel *cityLabel;
@property (weak, nonatomic) IBOutlet UILabel *stateLabel;
@property (weak, nonatomic) IBOutlet UILabel *countryLabel;
@property (weak, nonatomic) IBOutlet UILabel *phoneLabel;
@property (weak, nonatomic) IBOutlet UIImageView *categoryImageView;
@property (weak, nonatomic) IBOutlet MKMapView *map;
@end

@implementation RLDetailsViewController

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.view.backgroundColor = [UIColor colorWithRed:8/256.0 green:54/256.0 blue:67/256.0 alpha:1.0];
    self.title = @"Detalles";
    self.urlDescription = [url_1 stringByAppendingString:self.idUser];
    self.urlDescription = [self.urlDescription stringByAppendingString:url_2];
    NSLog(@"Url final: %@", self.urlDescription);
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithURL:[NSURL URLWithString:self.urlDescription] completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        NSLog(@"%@", json);
        self.description = [json valueForKeyPath:@"response.dataDescription"];
        if (!error) {
            NSError *jsonError;
            self.description = [[NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&jsonError] valueForKeyPath:@"response.venue"];
            [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                self.nameLabel.text = [self.description valueForKeyPath:@"name"];
                if(([self.description valueForKeyPath:@"location.address"]) == nil){
                    self.addressLabel.text = @"Direccion no disponible";
                }
                else{
                    self.addressLabel.text = [self.description valueForKeyPath:@"location.address"];
                }
                if (([self.description valueForKeyPath:@"location.city"]) == nil) {
                    self.cityLabel.text = @"Ciudad no disponible";
                }
                else{
                    self.cityLabel.text = [self.description valueForKeyPath:@"location.city"];
                }
                if(([self.description valueForKeyPath:@"location.state"]) == nil){
                    self.stateLabel.text = @"Estado no disponible";
                }
                else{
                    self.stateLabel.text = [self.description valueForKeyPath:@"location.state"];
                }
                if(([self.description valueForKeyPath:@"location.country"]) == nil){
                    self.countryLabel.text = @"Pais no disponible";
                }
                else{
                    self.countryLabel.text = [self.description valueForKeyPath:@"location.country"];
                }
                if(([self.description valueForKeyPath:@"contact.phone"]) == nil){
                    self.phoneLabel.text = @"Telefono no disponible";
                }
                else{
                    self.phoneLabel.text = [self.description valueForKeyPath:@"contact.phone"];
                }
                [self putVenueInterface:self.description];
            }];
        }
        else{
            NSLog(@"Hubo un error %@", [error localizedDescription]);
            [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                UIAlertView *alert =[[UIAlertView alloc] initWithTitle:@"Venues" message:@"No fue posible obtener la informacion" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
                [alert show];
            }];
        }
    }];
    [dataTask resume];
}
- (void)putVenueInterface:(NSDictionary *)venueInfo{
    NSArray *venueCategories = venueInfo[@"categories"];
    NSDictionary *category = [[venueCategories filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"primary == 1"]] lastObject];
    NSString *categoryIconURLString = [NSString stringWithFormat:@"%@bg_%d%@", [category valueForKeyPath:@"icon.prefix"],88,[category valueForKeyPath:@"icon.suffix"]];
    NSLog(@" %@", categoryIconURLString);
    [self.categoryImageView setImageWithURL:[NSURL URLWithString:categoryIconURLString] placeholderImage:[UIImage imageNamed:@"black-wallpaper-10"]];
}
-(void)viewDidLoad{
    [super viewDidLoad];
    
    
}
- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    CLLocationCoordinate2D coordinate = CLLocationCoordinate2DMake(self.latitude, self.longitude);
    MKCoordinateRegion region = MKCoordinateRegionMake(coordinate, MKCoordinateSpanMake(0.006f, 0.006f));
    [self.map setRegion:region animated:YES];
    
    RLVenueAnnotaion *annotation = [[RLVenueAnnotaion alloc] init];
    annotation.coordinate = coordinate;
    [self.map addAnnotation:annotation];
}

#pragma mark - MKMapViewDelegate Selectors
- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id<MKAnnotation>)annotation {
    if ([annotation isKindOfClass:[MKUserLocation class]])
        return nil;
    
    static NSString *annotationIdentifier = @"restaurantAnnotationIdentifier";
    MKPinAnnotationView *annotationView = (MKPinAnnotationView *)[mapView dequeueReusableAnnotationViewWithIdentifier:annotationIdentifier];
    
    if (!annotationView) {
        annotationView = [[MKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:annotationIdentifier];
        annotationView.pinColor = MKPinAnnotationColorRed;
    }
    else
        annotationView.annotation = annotation;
    
    return annotationView;
}
@end
