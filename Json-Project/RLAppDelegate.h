//
//  RLAppDelegate.h
//  Json-Project
//
//  Created by dcl17 on 21/01/14.
//  Copyright (c) 2014 dcl17. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RLViewController.h"

@interface RLAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
