//
//  RLDetailsViewController.h
//  Json-Project
//
//  Created by dcl17 on 22/01/14.
//  Copyright (c) 2014 dcl17. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RLDetailsViewController : UIViewController
@property (strong, nonatomic) NSString *idUser;
@property (assign, nonatomic) CGFloat latitude;
@property (assign, nonatomic) CGFloat longitude;
@end

