//
//  RLVenueAnnotaion.h
//  Json-Project
//
//  Created by dcl17 on 24/01/14.
//  Copyright (c) 2014 dcl17. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RLVenueAnnotaion : NSObject <MKAnnotation>
@property (nonatomic) CLLocationCoordinate2D coordinate;
@end
